//version inicial

var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function (req,res,next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept");
  next();
});

var requestjson = require('request-json');
var path = require('path');
var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3mb63921/collections/movimientos?apiKey=MVeLAjCn3CV55trs85QwUlUJeG5sdThB";
var urlUsuarios = "https://api.mlab.com/api/1/databases/bdbanca3mb63921/collections/usuarios?apiKey=MVeLAjCn3CV55trs85QwUlUJeG5sdThB";
var movimientosMlab = requestjson.createClient(urlMovimientos);
var usuariosMlab = requestjson.createClient(urlUsuarios);

app.listen(port);

console.log('todo listo RESTful API server started on: ' + port);

app.get('/',function(req, res){
    res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/', function(req,res){
    res.send('Su peticion ha sido recibida');
});

app.get('/clientes',function(req, res){
    res.sendFile(path.join(__dirname,'clientes.json'));
});

app.get('/clientes/:id',function(req,res) {
    res.send("aqui tiene el cliente numero: " + req.params.id);
});

app.post('/clientes', function(req,res){
    res.send('Sus clientes han sido dados de  Cambiado');
});

app.put('/clientes',function (req,res){
    res.send('Su informacion ha sido actualizada');
});

app.delete('/clientes',function (req,res){
    res.send('El cliente ha sido Eliminado');
});

app.get('/movimientos',function (req, res) {
  var clienteMlab = requestjson.createClient(urlMovimientos);
  clienteMlab.get('',function (err, resM, body) {
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
});

app.post('/movimientos',function (req, res) {
  movimientosMlab.post('',req.body,function (err, resM, body) {
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
});

app.post('/movimientos',function(req, res){
  movimientosMlab.post('',req.body, function(error, resM, body){
    console.log("req.body: " + req.body);
    console.log("error: " + error);
    console.log("resM: " + resM);
    console.log("body: " + body);
    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.send(body);
    }
  });
});

app.post('/altaMov',function(req, res){
  console.log("req.body: " + req.body);
  movimientosMlab.post('',req.body, function(error, resM, body){
    console.log("req.body: " + req.body);
    console.log("error: " + error);
    console.log("resM: " + resM.statusCode);
    console.log("body: " + body);
    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.send(body);
    }
  });
});

app.post('/altaClie',function(req, res){
  console.log("req.body: " + req.body);
  usuariosMlab.post('',req.body, function(error, resM, body){
    console.log("req.body: " + req.body);
    console.log("error: " + error);
    console.log("resM: " + resM.statusCode);
    console.log("body: " + body);
    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.status(resM.statusCode).send(body);
    }
  });
});

app.get('/consultaMov',function (req, res) {
  var clienteMlab = requestjson.createClient(urlMovimientos);
  clienteMlab.get('',function (err, resM, body) {
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
});

var orderna = '&s={"fecha": -1, "hora": -1}';
app.get('/consultaMov/:user',function (req, resp){
  console.log("req.params.user: " + req.query.user);
  var query = {};
  query.user = req.query.user;
  var ConsultaMov = urlMovimientos + "&q=" + JSON.stringify(query)+orderna;
  console.log("ConsultaMov: " + ConsultaMov);
  var ConsultaUsers = requestjson.createClient(ConsultaMov);
  ConsultaUsers.get('', function(error, resM, body){
    console.log("error: " + error);
    console.log("resM : " + resM.statusCode);
    console.log("body : " + body);

    if(error){
      console.log("error: " + error);
      return error;
    }else{
      console.log("body.length...: " + body.length);

      if(body.length == 0) {
        resp.status(404).send("Sin Informacion a listar");
      }else {
        resp.status(200).send(body);
      }
    }
  });
});





var sincontra = '&f={"password": 0}';
app.get('/validaClie',function (req, resp){
  console.log("query.user: " + req.query.user);
  console.log("query.pwd : " + req.query.password);
  var query = {};
  query.user = req.query.user;
  query.password = req.query.password;
  var urlConsultaUsuario = urlUsuarios + "&q=" + JSON.stringify(query) + sincontra;
  console.log("urlConsultaUsuario: " + urlConsultaUsuario);
  var ConsultaUsers = requestjson.createClient(urlConsultaUsuario);
  ConsultaUsers.get('', function(error, resM, body){
    console.log("error: " + error);
    console.log("resM : " + resM.statusCode);
    console.log("body : " + body);
    if(error){
      console.log("error: " + error);
      return error;
    }else{
      console.log("body.length...: " + body.length);
      if(body.length == 0) {
        resp.status(404).send("Sin Datos");
      }else {
        resp.status(200).send(body);
      }
    }
  });
});

var consultaClie=function(cliente){
  console.log("cliente: " + cliente);
  var query = {};
  query.user = cliente;
}
